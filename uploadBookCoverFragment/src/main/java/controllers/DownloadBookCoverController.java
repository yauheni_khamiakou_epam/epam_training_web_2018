package controllers;

import DAO.BookDAOFactory;
import beans.Book;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Controller for book images
 */
public class DownloadBookCoverController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Integer bookId = Integer.parseInt(request.getPathInfo().substring(1));
        Book book = new BookDAOFactory().getClassFromFactory().getBookById(bookId).get();
        File cover = book.getCover();
        if(cover != null){
            String coverName = cover.getName();
            InputStream coverStream = new FileInputStream(cover);
            try {
                response.setContentType(this.getServletContext().getMimeType(coverName));
                response.setContentLength(coverStream.available());
                response.setHeader("Content-Disposition", new StringBuilder("attachment; filename=\"")
                        .append(coverName)
                        .append("\"")
                        .toString());
                final byte[] bytes = new byte[1024];
                int bytesRead;
                while ((bytesRead = coverStream.read(bytes)) != -1) {
                    response.getOutputStream().write(bytes, 0, bytesRead);
                }
            } finally {
                coverStream.close();
            }
        }
    }
}
