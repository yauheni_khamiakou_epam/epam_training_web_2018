package controllers;

import database.DBConnector;
import fileWork.PartsExecutor;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Servlet for loading book covers
 */
@MultipartConfig
public class UploadBookCoverController extends HttpServlet {
    /**
    * Parameters
    */
    private static final String PARAM_ID = "bookId";
    private static final String ATTR_INFO = "info";
    /**
     * Queries
     */
    private static final String UPLOAD_COVER_QUERY = "UPDATE booksdb.books SET cover=? WHERE id=?";
    private static final int COVER_QUERY_PLACE = 1;
    private static final int ID_QUERY_PLACE = 2;
    /**
     * Exception msgs
     */
    private static final String CLOSING_SQL_EXCEPTION_MSG = "Exception while closing resources";
    private static final String LOADING_COVER_EXCEPTION_MSG = "Exception while loading book cover";
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(UploadBookCoverController.class.getName());
    /**
     * Constants
     */
    private static final String SAVE_FOLDER_INIT_NAME = "saveFolder";
    private static final char SLASH = '/';
    /**
     * Path
     */
    private static final String PATH_FORMATTING = "/formatting";

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String appPath = req.getServletContext().getRealPath("").replace('\\', UploadBookCoverController.SLASH);
        String saveFolder = this.getServletContext().getInitParameter(UploadBookCoverController.SAVE_FOLDER_INIT_NAME);
        StringBuilder resultPath = new StringBuilder(appPath);
        if(appPath.endsWith(UploadBookCoverController.SLASH + "")) {
            resultPath.append(saveFolder);
        } else {
            resultPath.append(UploadBookCoverController.SLASH).append(saveFolder);
        }
        File fileDir = new File(resultPath.toString());
        if(!fileDir.exists()) {
            fileDir.mkdir();
        }
        PartsExecutor executor = new PartsExecutor(req.getParts());
        String bookCoverName = executor.getFileName().get();
        resultPath.append(UploadBookCoverController.SLASH).append(bookCoverName);

        InputStream bookCoverStream = executor.getFileStream().get();
        File coverFile = new File(resultPath.toString());
        Files.copy(bookCoverStream, coverFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

        Integer bookId = Integer.parseInt(req.getParameter(UploadBookCoverController.PARAM_ID));
        try{
            this.loadCover(bookId, resultPath.toString());
        } catch (SQLException | NamingException e) {
            UploadBookCoverController.LOGGER.log(Level.SEVERE, UploadBookCoverController.LOADING_COVER_EXCEPTION_MSG, e);
            req.setAttribute(UploadBookCoverController.ATTR_INFO, UploadBookCoverController.LOADING_COVER_EXCEPTION_MSG);
        }
        resp.sendRedirect(req.getContextPath() + UploadBookCoverController.PATH_FORMATTING);
        //req.getRequestDispatcher(UploadBookCoverController.PATH_FORMATTING).forward(req, resp);
    }

    /**
     * Load book cover path in database
     * @param bookId - book id
     * @param bookCoverPath - book cover stream
     * @throws SQLException - in prepared statement
     * @throws NamingException - while making connection
     */
    private void loadCover(int bookId, String bookCoverPath) throws SQLException, NamingException {
        Connection connection = null;
        PreparedStatement statement = null;
        try{
            connection = new DBConnector().getConnection();
            statement = connection.prepareStatement(UploadBookCoverController.UPLOAD_COVER_QUERY);
            statement.setString(UploadBookCoverController.COVER_QUERY_PLACE, bookCoverPath);
            statement.setInt(UploadBookCoverController.ID_QUERY_PLACE, bookId);
            statement.execute();
            } finally {
                try {
                    if (statement != null){
                        statement.close();
                    }
                    if(connection != null){
                        connection.close();
                    }
                } catch (SQLException e){
                    UploadBookCoverController.LOGGER.log(Level.SEVERE, UploadBookCoverController.CLOSING_SQL_EXCEPTION_MSG, e);
                }
        }

    }
}
