package filters;

import fileWork.PartsExecutor;

import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Filter for chaining only images
 */
@MultipartConfig
public class ImageFileFilter implements Filter {
    /**
     * Parameters
     */
    private static final String PARAM_ID = "bookId";
    /**
     * Logging msg
     */
    private static final String LOGGING_MSG = "Loading cover for book ";
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(ImageFileFilter.class.getName());
    /**
     * Path
     */
    private static final String PATH_MAIN = "/main";
    /**
     * Attributes
     */
    private static final String IMAGE_ERROR_ATTR = "info";
    /**
     * Msgs
     */
    private static final String NO_IMAGE_MSG_LOG = "Filtering blocked. No image found";
    private static final String ENDING_FILTER_MSG_LOG = "Filtering ended";
    private static final String STARTING_FILTER_MSG_LOG = "Doing filter";
    private static final String IMAGE_ERROR_MSG = "Only .jpg/.jpeg can be uploaded";
    /**
     * Fields
     */
    private FilterConfig config;
    private Enumeration<String> formatNames;

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        this.log(req);
        ImageFileFilter.LOGGER.log(Level.INFO, ImageFileFilter.STARTING_FILTER_MSG_LOG);
        HttpServletRequest request = (HttpServletRequest) req;
        PartsExecutor partsExecutor = new PartsExecutor(request.getParts());
        Optional<String> fileName = partsExecutor.getFileName();
        if(fileName.isPresent() && this.isImage(fileName.get())) {
            ImageFileFilter.LOGGER.log(Level.INFO, ImageFileFilter.ENDING_FILTER_MSG_LOG);
            chain.doFilter(req, resp);
        } else {
            ImageFileFilter.LOGGER.log(Level.INFO, ImageFileFilter.NO_IMAGE_MSG_LOG);
            request.setAttribute(ImageFileFilter.IMAGE_ERROR_ATTR, ImageFileFilter.IMAGE_ERROR_MSG);
            request.getRequestDispatcher(ImageFileFilter.PATH_MAIN).forward(request, resp);
        }
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        this.config = config;
        this.formatNames = config.getInitParameterNames();
    }

    /**
     * Check whether file is image or not
     * @param fileName - file name
     * @return - result of checking
     */
    private boolean isImage(String fileName){
        boolean result = false;
        while (this.formatNames.hasMoreElements()) {
            String format = this.config.getInitParameter(this.formatNames.nextElement());
            String fileType = fileName.substring(fileName.length() - format.length());
            if(fileType.equals(format)){
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Log some info
     * @param request - servlet request
     */
    private void log(ServletRequest request) {
        String info = new StringBuilder(ImageFileFilter.LOGGING_MSG)
                .append(request.getParameter(ImageFileFilter.PARAM_ID))
                .toString();
        ImageFileFilter.LOGGER.info(info);
    }

}
