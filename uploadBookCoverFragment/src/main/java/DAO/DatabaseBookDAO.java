package DAO;

import beans.Book;
import database.DBConnector;
import exceptions.DAOException;

import javax.naming.NamingException;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Database DAO implementation
 */
public class DatabaseBookDAO implements IBookDAO{

    /**
     * Logger
     */
    private static final Logger logger = Logger.getLogger(DatabaseBookDAO.class.getName());
    /**
     * Constants
     */
    private static final String EMPTY_DESCRIPTION = "Description absents";
    /**
     * Queries
     */
    private static final String GET_BOOKS_QUERY = "SELECT * FROM booksdb.books;";
    private static final String INSERT_BOOK_QUERY = "INSERT INTO booksdb.books (title, author, description) VALUES(?,?,?);";
    private static final String UPDATE_BOOK_QUERY = "UPDATE booksdb.books SET title=?, author=?, description=? WHERE id=?;";
    private static final String GET_BOOK_BY_ID_QUERY = "SELECT * FROM booksdb.books WHERE id=?;";
    private static final String DELETE_BOOK_QUERY = "DELETE FROM booksdb.books WHERE id=?;";
    /**
     * Exception messages
     */
    private static final String SQL_EXCEPTION_MESSAGE = "SQL Exception in Database DAO implementation while ";
    private static final String NAMING_EXCEPTION_MESSAGE = "Naming Exception in Database DAO implementation while ";
    private static final String CLOSING_CONNECTION_MESSAGE = "closing connection ";
    private static final String GETTING_BOOKS_MESSAGE = "getting book list ";
    private static final String DELETING_BOOKS_MESSAGE = "deleting book ";
    private static final String GETTING_BOOK_BY_ID_MESSAGE = "getting book by id ";
    private static final String UPDATE_BOOK_MESSAGE = "updating book ";
    private static final String BOOK_NOT_FOUND_MESSAGE = "Update. Book not found ";
    private static final String NULL_BOOK_MESSAGE = "Null book detected ";
    /**
     * PreparedStatement indexes
     */
    private static final int TITLE_UPDATE_QUERY_PLACE = 1;
    private static final int AUTHOR_UPDATE_QUERY_PLACE = 2;
    private static final int DESCRIPTION_UPDATE_QUERY_PLACE = 3;
    private static final int ID_UPDATE_QUERY_PLACE = 4;
    private static final int TITLE_INSERT_QUERY_PLACE = 1;
    private static final int AUTHOR_INSERT_QUERY_PLACE = 2;
    private static final int DESCRIPTION_INSERT_QUERY_PLACE = 3;
    private static final int ID_DELETE_QUERY_PLACE = 1;
    /**
     * ResultSet argument indexes
     */
    private static final int ID_GET_QUERY_PLACE = 1;
    private static final int TITLE_GET_QUERY_PLACE = 2;
    private static final int AUTHOR_GET_QUERY_PLACE = 3;
    private static final int DESCRIPTION_GET_QUERY_PLACE = 4;
    private static final int COVER_GET_QUERY_PLACE = 5;

    @Override
    public List<Book> getBooks(){
        List<Book> books = new ArrayList<>();
        try(Connection connection = new DBConnector().getConnection();
            PreparedStatement statement = connection.prepareStatement(DatabaseBookDAO.GET_BOOKS_QUERY);
            ResultSet resultSet = statement.executeQuery();){
            while (resultSet.next()){
                books.add(this.getBookFromResultSet(resultSet).get());
            }
        } catch (SQLException e){
            String msg = new StringBuilder(DatabaseBookDAO.SQL_EXCEPTION_MESSAGE).append(DatabaseBookDAO.GETTING_BOOKS_MESSAGE).toString();
            logger.log(Level.SEVERE, msg, e);
        } catch (NamingException e) {
            String msg = new StringBuilder(DatabaseBookDAO.NAMING_EXCEPTION_MESSAGE).append(DatabaseBookDAO.GETTING_BOOKS_MESSAGE).toString();
            logger.log(Level.SEVERE, msg, e);
        }
        return books;
    }

    @Override
    public void saveBook(final Book book) throws DAOException {
        if(book == null){
            throw new DAOException(DatabaseBookDAO.NULL_BOOK_MESSAGE);
        } else {
            if(book.getId() == null){
                this.insertBook(book);
            } else {
                this.updateBook(book);
            }
        }
    }

    @Override
    public void insertBook(final Book book) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = new DBConnector().getConnection();
            statement = connection.prepareStatement(DatabaseBookDAO.INSERT_BOOK_QUERY);
            statement.setString(DatabaseBookDAO.TITLE_INSERT_QUERY_PLACE, book.getTitle());
            statement.setString(DatabaseBookDAO.AUTHOR_INSERT_QUERY_PLACE, book.getAuthor());
            statement.setString(DatabaseBookDAO.DESCRIPTION_INSERT_QUERY_PLACE, book.getDescription());
            statement.execute();
        } catch (SQLException e){
            String msg = new StringBuilder(DatabaseBookDAO.SQL_EXCEPTION_MESSAGE).append(DatabaseBookDAO.UPDATE_BOOK_MESSAGE).toString();
            logger.log(Level.SEVERE, msg, e);
        } catch (NamingException e) {
            String msg = new StringBuilder(DatabaseBookDAO.NAMING_EXCEPTION_MESSAGE).append(DatabaseBookDAO.UPDATE_BOOK_MESSAGE).toString();
            logger.log(Level.SEVERE, msg, e);
        } finally {
            this.closeResource(resultSet, statement, connection);
        }
    }

    @Override
    public void updateBook(final Book book) throws DAOException {
        if(this.getBookById(book.getId()).isPresent()){
            Connection connection = null;
            PreparedStatement statement = null;
            ResultSet resultSet = null;
            try {
                connection = new DBConnector().getConnection();
                statement = connection.prepareStatement(DatabaseBookDAO.UPDATE_BOOK_QUERY);
                statement.setString(DatabaseBookDAO.TITLE_UPDATE_QUERY_PLACE, book.getTitle());
                statement.setString(DatabaseBookDAO.AUTHOR_UPDATE_QUERY_PLACE, book.getAuthor());
                statement.setString(DatabaseBookDAO.DESCRIPTION_UPDATE_QUERY_PLACE, book.getDescription());
                statement.setInt(DatabaseBookDAO.ID_UPDATE_QUERY_PLACE, book.getId());
                statement.execute();
            } catch (SQLException e){
                String msg = new StringBuilder(DatabaseBookDAO.SQL_EXCEPTION_MESSAGE).append(DatabaseBookDAO.UPDATE_BOOK_MESSAGE).toString();
                logger.log(Level.SEVERE, msg, e);
            } catch (NamingException e) {
                String msg = new StringBuilder(DatabaseBookDAO.NAMING_EXCEPTION_MESSAGE).append(DatabaseBookDAO.UPDATE_BOOK_MESSAGE).toString();
                logger.log(Level.SEVERE, msg, e);
            } finally {
                this.closeResource(resultSet, statement, connection);
            }
        } else {
            throw new DAOException(DatabaseBookDAO.BOOK_NOT_FOUND_MESSAGE);
        }
    }

    @Override
    public Optional<Book> getBookById(final Integer id) {
        Optional<Book> book = Optional.empty();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = new DBConnector().getConnection();
            statement = connection.prepareStatement(DatabaseBookDAO.GET_BOOK_BY_ID_QUERY);
            statement.setInt(DatabaseBookDAO.ID_GET_QUERY_PLACE, id);
            resultSet = statement.executeQuery();
            if(resultSet.first()){
                book = this.getBookFromResultSet(resultSet);
            }
        } catch (SQLException e){
            String msg = new StringBuilder(DatabaseBookDAO.SQL_EXCEPTION_MESSAGE).append(DatabaseBookDAO.GETTING_BOOK_BY_ID_MESSAGE).toString();
            logger.log(Level.SEVERE, msg, e);
        } catch (NamingException e) {
            String msg = new StringBuilder(DatabaseBookDAO.NAMING_EXCEPTION_MESSAGE).append(DatabaseBookDAO.GETTING_BOOK_BY_ID_MESSAGE).toString();
            logger.log(Level.SEVERE, msg, e);
        } finally {
            this.closeResource(resultSet, statement, connection);
        }
        return book;
    }

    /**
     * Check book description and replace empty one if exists
     * @param description - input description
     * @return - sensible description
     */
    private String checkBookDescriptionAndChangeIfEmpty(final String description){
        String sensibleDescription = description;
        if("".equals(sensibleDescription) || sensibleDescription == null){
            sensibleDescription = DatabaseBookDAO.EMPTY_DESCRIPTION;
        }
        return sensibleDescription;
    }

    /**
     * Get book from resultSet
     * @param resultSet - actual resultSet
     * @return - optional of Book
     */
    private Optional<Book> getBookFromResultSet(final ResultSet resultSet) throws SQLException{
        int id  = resultSet.getInt(DatabaseBookDAO.ID_GET_QUERY_PLACE);
        String title = resultSet.getString(DatabaseBookDAO.TITLE_GET_QUERY_PLACE);
        String author = resultSet.getString(DatabaseBookDAO.AUTHOR_GET_QUERY_PLACE);
        String description = this.checkBookDescriptionAndChangeIfEmpty(resultSet.getString(DatabaseBookDAO.DESCRIPTION_GET_QUERY_PLACE));
        String coverFilePath = resultSet.getString(DatabaseBookDAO.COVER_GET_QUERY_PLACE);

        File cover = null;
        if(coverFilePath != null && !"".equals(coverFilePath)) {
            cover = new File(coverFilePath);
            if (!cover.exists() && !cover.isFile()) {
                cover = null;
            }
        }
        return Optional.of(new Book(id, title, author, description, cover));
    }

    @Override
    public void deleteBook(final int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = new DBConnector().getConnection();
            statement = connection.prepareStatement(DatabaseBookDAO.DELETE_BOOK_QUERY);
            statement.setInt(DatabaseBookDAO.ID_DELETE_QUERY_PLACE, id);
            statement.execute();
        } catch (SQLException e){
            String msg = new StringBuilder(DatabaseBookDAO.SQL_EXCEPTION_MESSAGE).append(DatabaseBookDAO.DELETING_BOOKS_MESSAGE).toString();
            logger.log(Level.SEVERE, msg, e);
        } catch (NamingException e) {
            String msg = new StringBuilder(DatabaseBookDAO.NAMING_EXCEPTION_MESSAGE).append(DatabaseBookDAO.DELETING_BOOKS_MESSAGE).toString();
            logger.log(Level.SEVERE, msg, e);
        } finally {
            this.closeResource(statement, connection);
        }
    }

    /**
     * Help to close resources
     * @param autoCloseables - resources
     */
    private void closeResource(final AutoCloseable... autoCloseables){
        try {
            for (final AutoCloseable autoCloseable : autoCloseables) {
                if (autoCloseable != null) {
                    autoCloseable.close();
                }
            }
        } catch (Exception e) {
            String msg = new StringBuilder(DatabaseBookDAO.SQL_EXCEPTION_MESSAGE).append(DatabaseBookDAO.CLOSING_CONNECTION_MESSAGE).toString();
            logger.log(Level.SEVERE, msg, e);
        }
    }
}
