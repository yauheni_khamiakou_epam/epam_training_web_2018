package DAO;

import beans.Book;
import exceptions.DAOException;

import java.util.List;
import java.util.Optional;

/**
 * Book DAO interface
 */
public interface IBookDAO {

    /**
     * @return list of books
     */
    List<Book> getBooks();

    /**
     * @param book to insert
     * @throws DAOException if something went wrong
     */
    void insertBook(final Book book) throws DAOException;

    /**
     * @param book to update
     * @throws DAOException if something went wrong
     */
    void updateBook(final Book book) throws DAOException;

    /**
     * Serve insert or update depends on id
     * @param book - book to serve
     * @throws DAOException if something wrong with book
     */
    void saveBook(final Book book) throws DAOException;

    /**
     * Get book by its id
     * @param id - book id
     * @return - book
     */
    Optional<Book> getBookById(final Integer id);

    /**
     * Delete book from base
     * @param id - book id
     */
    void deleteBook(final int id);
}
