package ykhamiakou.main.beans;

import java.io.File;
import java.io.InputStream;

/**
 * Some book
 */
public class Book {
    //Fields
    private Integer id;
    private String title;
    private String author;
    private String description;
    private File cover;

    /**
     * Full constructor
     * @param id - book id
     * @param title - book title
     * @param author - book author
     * @param description - book description
     * @param cover - book cover file
     */
    public Book(Integer id, String title, String author, String description, File cover) {
        this.id = id;
        this.title = title;
        this.author = author;
        this. description = description;
        this.cover = cover;
    }

    /**
     * @return book id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @return book title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return book author
     */
    public String getAuthor() {
        return author;
    }

    /**
     * @return description to get
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return book cover file
     */
    public File getCover() {
        return cover;
    }

}
