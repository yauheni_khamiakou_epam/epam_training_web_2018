package ykhamiakou.main.database;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Data base connection pool
 */
public class DBConnector {
    /**
     * Constants
     */
    private static final String BASE_PATH = "jdbc/booksdb";

    /**
     * Gets connection to data base
     *
     * @return - connection
     * @throws NamingException
     * @throws SQLException
     */
    public Connection getConnection() throws NamingException, SQLException {
        final Context context = (Context) new InitialContext().lookup("java:/comp/env");
        final DataSource dataSource = (DataSource) context.lookup(DBConnector.BASE_PATH);
        return dataSource.getConnection();
    }

}
