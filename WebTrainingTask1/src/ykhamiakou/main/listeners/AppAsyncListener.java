package ykhamiakou.main.listeners;

import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Application async listener
 */
public class AppAsyncListener implements AsyncListener {
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(AppAsyncListener.class.getName());
    /**
     * Messages
     */
    private static final String ON_COMPLETE_MSG = "Async completed";
    private static final String ON_TIMEOUT_MSG = "Async timeout";
    private static final String ON_START_MSG = "Async started";

    @Override
    public void onComplete(AsyncEvent asyncEvent) throws IOException {
        LOGGER.log(Level.INFO, AppAsyncListener.ON_COMPLETE_MSG);
    }

    @Override
    public void onTimeout(AsyncEvent asyncEvent) throws IOException {
        LOGGER.log(Level.INFO, AppAsyncListener.ON_TIMEOUT_MSG);
    }

    @Override
    public void onError(AsyncEvent asyncEvent) throws IOException {
        AsyncContext asyncContext = asyncEvent.getAsyncContext();
        if(asyncContext.getRequest().isAsyncStarted()){
            asyncContext.complete();
        }
        Exception exception = (Exception) asyncEvent.getThrowable();
        LOGGER.log(Level.SEVERE, exception.getMessage(), exception);
    }

    @Override
    public void onStartAsync(AsyncEvent asyncEvent) throws IOException {
        LOGGER.log(Level.INFO, AppAsyncListener.ON_START_MSG);
    }
}
