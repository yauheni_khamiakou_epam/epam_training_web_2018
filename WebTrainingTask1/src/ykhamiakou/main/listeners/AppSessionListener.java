package ykhamiakou.main.listeners;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Listener for session. Log information when session was activated and deactivated.
 */
public class AppSessionListener implements HttpSessionListener {
    /**
     * Constants
     */
    private static int SESSION_INACTIVE_INTERVALE = 1000;
    /**
     * Logger
     */
    private static final Logger logger = Logger.getLogger(AppSessionListener.class.getName());
    /**
     * Message constants
     */
    private static final String SESSION_CREATED_MSG = " Session created.";
    private static final String SESSION_DESTROYED_MSG = " Session destroyed. Session lifetime :";
    private static final String TIME_VALUE = " sec.";

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        httpSessionEvent.getSession().setMaxInactiveInterval(AppSessionListener.SESSION_INACTIVE_INTERVALE);
        String msg = new StringBuilder(new Date().toString()).append(AppSessionListener.SESSION_CREATED_MSG).toString();
        logger.log(Level.INFO, msg);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        Date nowDate = new Date();
        long sessionLifeTime = nowDate.getTime() - httpSessionEvent.getSession().getCreationTime();
        String msg = new StringBuilder(nowDate.toString()).append(AppSessionListener.SESSION_DESTROYED_MSG)
                .append(TimeUnit.MILLISECONDS.toSeconds(sessionLifeTime)).append(AppSessionListener.TIME_VALUE).toString();
        logger.log(Level.INFO, msg);
    }
}
