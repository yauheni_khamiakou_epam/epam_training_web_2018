package ykhamiakou.main.listeners;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.logging.Logger;

/**
 * Application context listener. Log some application info
 */
public class AppContextListener implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(AppContextListener.class.getName());

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        this.log(servletContextEvent.getServletContext(), "Application started");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        this.log(servletContextEvent.getServletContext(), "Application stopped");
    }

    /**
     * Take a log
     * @param context - application context
     * @param msg - message to log
     */
    protected void log(ServletContext context, String msg){
        String serverInfo = context.getServerInfo();
        String applicationName = context.getServletContextName();
        StringBuilder logBuilder = new StringBuilder().append(serverInfo).append(": Application ").append(applicationName).append(" : ").append(msg);
        logger.info( logBuilder.toString());
    }
}
