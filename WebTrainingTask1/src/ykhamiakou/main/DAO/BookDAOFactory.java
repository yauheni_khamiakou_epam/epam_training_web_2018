package ykhamiakou.main.DAO;

/**
 * Factory for book DAO
 */
public class BookDAOFactory {

    /**
     * @return book DAO
     */
    public IBookDAO getClassFromFactory(){
        return new DatabaseBookDAO();
    }
}
