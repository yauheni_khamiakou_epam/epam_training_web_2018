package ykhamiakou.main.exceptions;

/**
 * Exception if empty values are detected
 */
public class EmptyValuesException extends Exception {
    /**
     * Serial version
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor with args
     *
     * @param message
     *            - exception message
     */
    public EmptyValuesException(final String message) {
        super(message);
    }

}
