package ykhamiakou.main.controllers;

import ykhamiakou.main.listeners.AppAsyncListener;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Second async attribute controller
 */
public class AsyncSecondAttributeController extends HttpServlet {
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(AsyncSecondAttributeController.class.getName());
    /**
     * Path
     */
    private static final String RESULT_PAGE_PATH = "/async.jsp";
    /**
     * Attributes
     */
    private static final String SECOND_ATTR_NAME = "secondAsyncAttr";
    private static final String SECOND_ATTR_VALUE = " Second async controller executed.";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        AsyncContext asyncContext = req.startAsync();
        asyncContext.addListener(new AppAsyncListener());
        asyncContext.start(() -> {
            asyncContext.getRequest().setAttribute(AsyncSecondAttributeController.SECOND_ATTR_NAME,
                    new StringBuilder(new Date().toString()).append(AsyncSecondAttributeController.SECOND_ATTR_VALUE));
            asyncContext.dispatch(AsyncSecondAttributeController.RESULT_PAGE_PATH);
        });
    }
}
