package ykhamiakou.main.controllers;

import ykhamiakou.main.DAO.BookDAOFactory;
import ykhamiakou.main.beans.Book;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Formatting controller. Select books for formatting page
 */
public class FormattingBookController extends AbstractController {
    /**
     * Attributes
     */
    private static final String ATTR_BOOKS = "books";
    /**
     * Path
     */
    private static final String PATH_FORMATTING = "/formatting.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Book> books = new BookDAOFactory().getClassFromFactory().getBooks();
        req.setAttribute(FormattingBookController.ATTR_BOOKS, books);
        req.getRequestDispatcher(FormattingBookController.PATH_FORMATTING).forward(req, resp);
    }
}
