package ykhamiakou.main.controllers;

import ykhamiakou.main.listeners.AppAsyncListener;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * First async attribute controller
 */
public class AsyncFirstAttributeController extends HttpServlet {
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(AsyncFirstAttributeController.class.getName());
    /**
     * Parameters
     */
    private static final String TIME_PARAM_NAME = "time";
    /**
     * Path
     */
    private static final String SECOND_ASYNC_ATTR_CONTROLLER_PATH = "/asyncSecAttr";
    /**
     * Attributes
     */
    private static final String FIRST_ATTR_NAME = "firstAsyncAttr";
    private static final String FIRST_ATTR_VALUE = " First async controller executed.";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int waitSeconds = Integer.parseInt(req.getParameter(AsyncFirstAttributeController.TIME_PARAM_NAME));
        AsyncContext asyncContext = req.startAsync();
        asyncContext.addListener(new AppAsyncListener());
        asyncContext.start(() -> {
            try{
                asyncContext.getRequest().setAttribute(AsyncFirstAttributeController.FIRST_ATTR_NAME,
                        new StringBuilder(new Date().toString()).append(AsyncFirstAttributeController.FIRST_ATTR_VALUE));
                Thread.sleep(waitSeconds);
                asyncContext.dispatch(AsyncFirstAttributeController.SECOND_ASYNC_ATTR_CONTROLLER_PATH);
            } catch (InterruptedException e) {
                LOGGER.log(Level.SEVERE, e.getMessage(), e);
            }
        });
    }
}
