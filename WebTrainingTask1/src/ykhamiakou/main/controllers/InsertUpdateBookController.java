package ykhamiakou.main.controllers;

import ykhamiakou.main.DAO.BookDAOFactory;
import ykhamiakou.main.DAO.IBookDAO;
import ykhamiakou.main.beans.Book;
import ykhamiakou.main.exceptions.DAOException;
import ykhamiakou.main.exceptions.EmptyValuesException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller for inserting and updating book
 */
public class InsertUpdateBookController extends AbstractController {
    /**
     * Constants
     */
    private static final String ENCODING = "UTF-8";
    private static final Lock LOCK = new ReentrantLock();
    /**
     * Logger
     */
    private static final Logger LOGGER = Logger.getLogger(InsertUpdateBookController.class.getName());
    /**
     * Exception messages
     */
    private static final String EMPTY_PARAMETERS_MESSAGE = "Fill in all the inputs!";
    /**
     * Parameters
     */
    private static final String PARAM_ID = "bookId";
    private static final String PARAM_TITLE = "bookTitle";
    private static final String PARAM_AUTHOR = "bookAuthor";
    private static final String PARAM_DESCRIPTION = "bookDescription";
    /**
     * Path
     */
<<<<<<< HEAD
    private static final String PATH_MAIN = "/formatting";
=======
    private static final String PATH_MAIN = "/main";
>>>>>>> remotes/origin/security

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            req.setCharacterEncoding(InsertUpdateBookController.ENCODING);
            String stringId = req.getParameter(InsertUpdateBookController.PARAM_ID);
            String title = req.getParameter(InsertUpdateBookController.PARAM_TITLE);
            String author = req.getParameter(InsertUpdateBookController.PARAM_AUTHOR);
            String description = req.getParameter(InsertUpdateBookController.PARAM_DESCRIPTION);
            if(!super.isNotEmptyParameters(title, author, description)){
                throw new EmptyValuesException(InsertUpdateBookController.EMPTY_PARAMETERS_MESSAGE);
            }

            InsertUpdateBookController.LOCK.lock();
            try {
                IBookDAO dao = new BookDAOFactory().getClassFromFactory();
                Integer id = null;
                if(!"".equals(stringId)){
                    id = Integer.parseInt(stringId);
                }
                dao.saveBook(new Book(id, title,
                        author, description, null));
            } finally {
                InsertUpdateBookController.LOCK.unlock();
            }
            resp.sendRedirect(InsertUpdateBookController.PATH_MAIN);
        } catch (DAOException e) {
            super.logExceptionAndForward(InsertUpdateBookController.LOGGER, req, resp, e, InsertUpdateBookController.PATH_MAIN);
        } catch (EmptyValuesException e){
            super.logExceptionAndForward(InsertUpdateBookController.LOGGER, req, resp, e, InsertUpdateBookController.PATH_MAIN);
        }
    }
}
