package ykhamiakou.main.controllers;

import ykhamiakou.main.DAO.BookDAOFactory;
import ykhamiakou.main.beans.Book;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Main page controller
 */
public class MainBookController extends AbstractController {
    /**
     * Attributes
     */
    private static final String ATTR_BOOKS = "books";
    /**
     * Path
     */
    private static final String PATH_MAIN = "/main.jsp";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.execute(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.execute(req, resp);
    }

    /**
     * Set book list in request and forward to main page
     * @param req - request
     * @param resp - response
     * @throws ServletException
     * @throws IOException
     */
    private void execute(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
        List<Book> books = new BookDAOFactory().getClassFromFactory().getBooks();
        req.setAttribute(MainBookController.ATTR_BOOKS, books);
        req.getRequestDispatcher(MainBookController.PATH_MAIN).forward(req, resp);
        if(!super.available && !super.isAvailable(this.getServletConfig())){
            super.makeUnavailable(this.getServletConfig());
        }
    }
}
