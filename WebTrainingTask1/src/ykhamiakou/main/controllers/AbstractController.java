package ykhamiakou.main.controllers;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract controller
 */
public abstract class AbstractController extends HttpServlet {
    /**
     * Attributes
     */
    private static final String ATTR_INFO = "info";
    /**
     * Fields
     */
    protected boolean available;

    /**
     * Check whether servlet is available or not
     * @param config - servlet config
     * @return - true or false
     */
    protected boolean isAvailable(ServletConfig config){
        if(!Boolean.parseBoolean(config.getInitParameter("isAvailable"))){
            return false;
        }
        return true;
    }

    /**
     * Method to throw an UnavailableException and make current servlet unavailable
     * @param config - servlet config
     * @throws UnavailableException
     */
    protected void makeUnavailable(ServletConfig config) throws UnavailableException{
        if(!this.available){
            StringBuilder msgBuilder = new StringBuilder().append(config.getServletName()).append(" is unavailable");
            int unavailableTime = Integer.parseInt(config.getInitParameter("unavailableTime"));
            if(unavailableTime > 0){
                this.available = true;
            }
            throw new UnavailableException(msgBuilder.toString(), unavailableTime);
        }
    }

    /**
     * Log catched exception, log it and forward to some path
     * @param logger - logger for logging msg
     * @param req - servlet request
     * @param resp - servlet response
     * @param e - catched exception
     * @param forwardPath - path to forward
     * @throws ServletException - servlet exception
     * @throws IOException - i/o exception
     */
    protected void logExceptionAndForward(Logger logger, HttpServletRequest req, HttpServletResponse resp, Exception e, String forwardPath) throws ServletException, IOException {
        logger.log(Level.WARNING, e.getMessage(), e);
        req.setAttribute(AbstractController.ATTR_INFO, e.getMessage());
        req.getRequestDispatcher(forwardPath).forward(req, resp);
    }

    /**
     * Check whether request parameters are empty or not
     * @param parameters - request parameters
     * @return - check result
     */
    protected boolean isNotEmptyParameters(final String ... parameters){
        boolean checkResult = true;
        for(String parameter: parameters){
            if("".equals(parameter) || parameter == null){
                checkResult = false;
                break;
            }
        }
        return checkResult;
    }
}
