package ykhamiakou.main.controllers;

import ykhamiakou.main.DAO.BookDAOFactory;
import ykhamiakou.main.DAO.IBookDAO;
import ykhamiakou.main.exceptions.EmptyValuesException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

/**
 * Controller for deleting book
 */
public class DeleteBookController extends AbstractController {
    /**
     * Constants
     */
    private static final Logger LOGGER = Logger.getLogger(DeleteBookController.class.getName());
    private static final Lock LOCK = new ReentrantLock();
    /**
     * Exception messages
     */
    private static final String EMPTY_PARAMETERS_MESSAGE = "Fill in all the inputs!";
    /**
     * Parameters
     */
    private static final String PARAM_ID = "deleteBookId";
    /**
     * Path
     */
    private static final String PATH_MAIN = "/formatting";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String bookID = req.getParameter(DeleteBookController.PARAM_ID);
            if(!super.isNotEmptyParameters(bookID)){
                throw new EmptyValuesException(DeleteBookController.EMPTY_PARAMETERS_MESSAGE);
            }
            DeleteBookController.LOCK.lock();
            try{
                int id = Integer.parseInt(bookID);
                IBookDAO dao = new BookDAOFactory().getClassFromFactory();
                dao.deleteBook(id);
            } finally {
                DeleteBookController.LOCK.unlock();
            }
            resp.sendRedirect(DeleteBookController.PATH_MAIN);
        } catch (EmptyValuesException e){
            super.logExceptionAndForward(DeleteBookController.LOGGER, req, resp, e, DeleteBookController.PATH_MAIN);
        }
    }
}
