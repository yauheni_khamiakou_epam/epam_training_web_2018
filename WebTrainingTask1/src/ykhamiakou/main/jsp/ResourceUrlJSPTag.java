package ykhamiakou.main.jsp;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.net.URL;

/**
 * Jsp tag class for resolving appropriate web component for the posted url
 */
public class ResourceUrlJSPTag extends SimpleTagSupport {
    /**
     * Constants
     */
    private static final String URL_ATTR = "url";
    private static final String SERVLET_ATTR = "resource";
    /**
     * Fields
     */
    private String url;

    @Override
    public void doTag() throws JspException, IOException {
        JspContext jspContext = this.getJspContext();
        PageContext pageContext = (PageContext)jspContext;
        ServletContext servletContext = pageContext.getServletContext();
        jspContext.setAttribute(ResourceUrlJSPTag.URL_ATTR, this.url);
        jspContext.setAttribute(ResourceUrlJSPTag.SERVLET_ATTR, servletContext.getRealPath(this.url));

        this.getJspBody().invoke(null);
    }

    /**
     * @param url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return url param
     */
    public String getUrl() {
        return url;
    }
}
