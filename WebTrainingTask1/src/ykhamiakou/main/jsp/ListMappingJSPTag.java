package ykhamiakou.main.jsp;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Class for jsp tag for listing all the urls mapping
 */
public class ListMappingJSPTag extends TagSupport {
    /**
     * Constants
     */
    private static final String MAPPING_SPLITTER = " ; ";
    private static final String URL_ATTR = "url";
    private static final String SERVLET_ATTR = "servlet";

    /**
     * Mapping
     */
    private HashMap<String, String> mapping;
    private Iterator<Map.Entry<String, String>> entryIterator;

    @Override
    public int doStartTag() throws JspException {
        ServletContext context = this.pageContext.getServletContext();
        Map <String, ? extends ServletRegistration> registrations = context.getServletRegistrations();
        this.mapping = new HashMap<>();
        for(Map.Entry<String, ? extends ServletRegistration> entry: registrations.entrySet()){
            this.mapping.put(entry.getKey(), this.registrationMappingsToString(entry.getValue()));
        }
        this.entryIterator = this.mapping.entrySet().iterator();
        return TagSupport.EVAL_BODY_INCLUDE;
    }

    /**
     * Create mapping string
     * @param registration - servlet registration
     * @return - mappings in string
     */
    private String registrationMappingsToString(ServletRegistration registration) {
        Collection<String> servletMapping = registration.getMappings();
        StringBuilder mappingStringBuilder = new StringBuilder();
        for(String string: servletMapping){
            mappingStringBuilder.append(string).append(ListMappingJSPTag.MAPPING_SPLITTER);
        }
        return mappingStringBuilder.toString();
    }

    @Override
    public int doEndTag() throws JspException {
        return TagSupport.EVAL_PAGE;
    }

    @Override
    public int doAfterBody() throws JspException {
        if(this.entryIterator.hasNext()) {
            HttpServletRequest request = (HttpServletRequest) this.pageContext.getRequest();
            Map.Entry<String, String> entry = this.entryIterator.next();
            request.setAttribute(ListMappingJSPTag.URL_ATTR, entry.getValue());
            request.setAttribute(ListMappingJSPTag.SERVLET_ATTR, entry.getKey());
            return  TagSupport.EVAL_BODY_AGAIN;
        }
        return TagSupport.SKIP_BODY;
    }
}
