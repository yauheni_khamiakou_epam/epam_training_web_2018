Security task

Added FORM authentication.
All book formatting functionality removed to formatting.jsp (adding, updating, deleting books, uploading covers).
Formatting functionality is allowed to user_management role.