<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="utils" uri="/tld/UtilsJSPTagDescriptor.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Test Custom tags</title>
</head>
<body>
    <h3>SERVLET MAPPING</h3>
    <utils:listmapping>
        ${url} - ${servlet}
        <br>
    </utils:listmapping>

    <h3>URL RESOURCE</h3>
    <utils:resolveurl url="/main">
        ${url} - ${resource}
    </utils:resolveurl>
</body>
</html>
