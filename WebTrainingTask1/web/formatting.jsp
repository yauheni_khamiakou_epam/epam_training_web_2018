<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Formatting</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="script/pageScript.js"></script>
</head>
<body>
<article>

    <a href="/main">To Main</a>
    <br>
    <header>
        <b>Book list</b>
    </header>

    <c:forEach items="${books}" var="book" varStatus="container">
        <c:if test="${book.cover eq null}">
            <form action="/uploadCover" name="coverForm" id="bookLoadCoverFormId${book.id}" method="post" enctype="multipart/form-data">
                <input name="bookId" id="bookLoadCoverId${book.id}" type="hidden">
                <input type="file" name="file">
                <input type="reset" value="Reset">
                <input type="button" value="Load" onclick="JavaScript:loadCover('${book.id}')">
            </form>
        </c:if>

        <c:if test="${book.cover ne null}">
            <img src="${pageContext.request.contextPath}/images/${book.id}" height="100px">
        </c:if>

        <p>${book.id}; ${book.title}; ${book.author}; <br>${book.description}</p>
        <br>
        <form action="/delete" name="deleteForm" id="deleteFormId${book.id}">
            <input name="deleteBookId" id="deleteBookId${book.id}" type="hidden">
            <input type="button" value="Delete" onclick="JavaScript:deleteBook('${book.id}')">
        </form>
        <hr>
    </c:forEach>

    <section>
        <a href="JavaScript:changeActionToUpdate()">Update book</a>
        <a href="JavaScript:changeActionToInsert()">Insert book</a>
        <form action="serveBook" id="updateBookForm" method="get">
            <p id="actionTitle">Update</p>
            <input type="text" id="bookId" name="bookId" placeholder="Id" />
            <input type="text" id="bookTitle" name="bookTitle" placeholder="Title" />
            <input type="text" id="bookAuthor" name="bookAuthor" placeholder="Author" />
            <textarea rows="4" type="text" id="bookDescription" name="bookDescription" placeholder="Description"></textarea>
            <input class="button" type="submit" value="Submit" />
        </form>
        <c:if test="${info ne null}">
            ${info}
        </c:if>
    </section>
</article>
</body>
</html>
