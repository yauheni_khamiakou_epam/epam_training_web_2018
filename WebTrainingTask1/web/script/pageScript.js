/**
 * Change form action to update book
 */
function changeActionToUpdate() {
    changeText('actionTitle', 'Update');
    showElement('bookId');
}

/**
 * Change form action to insert book
 */
function changeActionToInsert() {
    changeText('actionTitle', 'Insert');
    hideElement('bookId');
}

/**
 * Change text in some element
 * @param id - div id
 * @param text - new text
 */
function changeText(id, text) {
    document.getElementById(id).innerHTML = text;
}

/**
 * Hide some element by its id and
 * @param id - element id
 */
function hideElement(id) {
    document.getElementById(id).style.visibility = "hidden";
}

/**
 * Show some element by its id
 * @param id - element id
 */
function showElement(id) {
    document.getElementById(id).style.visibility = "visible";
}

/**
 * Load book cover
 * @param id - book to load cover
 */
function loadCover(id){
    document.getElementById("bookLoadCoverId" + id).value = id;
    document.getElementById("bookLoadCoverFormId" + id).submit();
}

/**
 * Delete book
 * @param id - book to delete
 */
function deleteBook(id) {
    document.getElementById("deleteBookId" + id).value = id;
    document.getElementById("deleteFormId" + id).submit();
}