<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta content="text/html; charset=utf-8">
    <title>Async</title>
</head>
<body>
<article>
    <header>
        <b>Test async</b>
    </header>

    <form id="asyncForm" action="/async" method="get">
        <input type="number" id="time" name="time" placeholder="Thread sleep time">
        <input type="submit" value="Test">
    </form>

    <c:if test="${firstAsyncAttr ne null}">
        ${firstAsyncAttr}<br>
    </c:if>
    <c:if test="${secondAsyncAttr ne null}">
        ${secondAsyncAttr}<br>
    </c:if>

    <br>
    <a href="/main">To main</a>
</article>
</body>
</html>
