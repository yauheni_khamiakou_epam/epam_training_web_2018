<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta content="text/html; charset=utf-8">
    <title>Main</title>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="script/pageScript.js"></script>
</head>
<body>
<article>
    <a href="/async.jsp">Test async</a>
    <a href="/tags.jsp">Test custom tags</a>
    <a href="/formatting">Format Books</a>
    <br>
    <br>
    <header>
        <b>Book list</b>
    </header>
    <c:forEach items="${books}" var="book" varStatus="container">

        <c:if test="${book.cover ne null}">
            <img src="${pageContext.request.contextPath}/images/${book.id}" height="100px">
        </c:if>

        <p>${book.id}; ${book.title}; ${book.author}; <br>${book.description}</p>
        <hr>
    </c:forEach>

<<<<<<< HEAD
<<<<<<< HEAD

    <section>
        <a href="JavaScript:changeActionToUpdate()">Update book</a>
        <a href="JavaScript:changeActionToInsert()">Insert book</a>
        <form action="serveBook" id="updateBookForm" method="get">
            <p id="actionTitle">Update</p>
            <input type="text" id="bookId" name="bookId" placeholder="Id" />
            <input type="text" id="bookTitle" name="bookTitle" placeholder="Title" />
            <input type="text" id="bookAuthor" name="bookAuthor" placeholder="Author" />
            <textarea rows="4" type="text" id="bookDescription" name="bookDescription" placeholder="Description"></textarea>
            <input class="button" type="submit" value="Submit" />
        </form>
        <c:if test="${info ne null}">
            ${info}
        </c:if>
    </section>

    <br>
    <a href="/async.jsp">Test async</a>

    <br>
    <a href="/tags.jsp">Test custom tags</a>
=======
>>>>>>> 2ac09bcc6408dce42b9c7132b8230a41bc97b098
=======
>>>>>>> remotes/origin/security
</article>
</body>
</html>
